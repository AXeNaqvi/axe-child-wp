<?php
if(!defined('AXECTHEME')){
	exit('What are you doing here??');
}

class childActions{

	public static function axechild_loadcss(){
		wp_dequeue_style('themeaxe-style-css');
		wp_enqueue_style('themeaxe-maincss', THEMEAXEPATH.'/style.css', false, THEMEAXEVER,false);
		wp_enqueue_style(AXECTHEME . '-style-css', CURRENTTHEMEAXEPATH.'/style.css', false, CTHEMEVER,false);
	}
	public static function axechild_loadjs(){
		wp_dequeue_script('jquery-effects-core');
		wp_enqueue_script('jquery-effects-core');
		wp_enqueue_script('themeaxe-child-js', CURRENTTHEMEAXEPATH.'/js/script.js', array('jquery'), CTHEMEVER,true);
	}

}
?>