<?php

define('CTHEMEVER', '1.0.0');
define('AXECTHEME', 'axe-child-wp');
require_once get_template_directory() . '/includes/constants.php';

add_action('after_setup_theme', 'axechild_setup');
function axechild_setup()
{
    axe_remove_child_actions();
    axe_add_child_actions();
    axe_remove_child_filters();
    axe_add_child_filters();
    axe_add_child_shortcodes();
}

function axe_remove_child_actions()
{
}
function axe_add_child_actions()
{
    add_action('wp_enqueue_scripts', array('childActions', 'axechild_loadcss'));
    add_action('wp_enqueue_scripts', array('childActions', 'axechild_loadjs'), 100);
}
function axe_remove_child_filters()
{

}
function axe_add_child_filters()
{

}
function axe_add_child_shortcodes()
{
}

function themeaxe_ContentCaller($file, $once = false)
{
    if (file_exists(CURRENTTHEMEAXEROOT . '/' . $file . '.php')) {
        $file = CURRENTTHEMEAXEROOT . '/' . $file . '.php';

    } else if (file_exists(THEMEAXEROOT . '/' . $file . '.php')) {
        $file = THEMEAXEROOT . '/' . $file . '.php';

    } else if (!file_exists($file)) {
        $file = '';
    }

    if (!empty($file)) {
        if ($once) {
            include_once $file;
        } else {
            include $file;
        }
    }

}

$axechildincludesarray = array('actions', 'filters', 'shortcodes');
foreach ($axechildincludesarray as $key) {
    get_template_part('includes/' . $key);
}

function themeaxe_CustomCSS()
{
    $typography = (array) themeaxe_GetAllThemeSettings('typography');
    $general = themeaxe_GetAllThemeSettings('general');
    $style = '';
    $tabletstyle = '';
    $mobilestyle = '';
    $style .= themeaxe_gFontFace();
    $style .= '<style type="text/css">';

    /* @fontface here */
    $style .= themeaxe_myFontFace();
    /* @fontface here */

    /* Style */
    $container = '';
    $other = '';

    if (axeHelper::isValidArray($typography)) {
        foreach ($typography as $key => $val) {

            switch ($val->alias) {
                case 'h1':
                $container = '';
                $sty = '';

                break;
                case 'h2':
                $container = '';
                $sty = '';
                break;
                case 'h6':
                $container = 'h2.comments-title,h2.comment-reply-title,';
                $sty = '';
                break;
                case 'a:hover':
                $container = '.axemainmenu ul li:hover > a, .axemainmenu ul li.current_page_item > a,.axemainmenu ul li.current_page_parent > a,.axemainmenu ul li.current_page_ancestor > a,';
                $sty = 'border-color:#' . $val->color . '!important;';
                break;
                case 'body':
                $sty = '';
                break;
                default:
                $container = '';
                $sty = '';
                break;
            }

            $fonts = themeaxe_fonts();
            $fselect = null;
            if (isset($fonts[strtolower($val->font)])) {
                $fselect = $fonts[strtolower($val->font)];
            }
            if (!$fselect) {
                if (isset($fonts[$val->font])) {
                    $fselect = $fonts[$val->font];
                }
            }

            if ($fselect) {
                $style .= $container . $val->alias . '{';
                $style .= 'font-family:' . $fselect['family'] . ';';
                $style .= 'font-size:' . $val->size . 'px;';
                $style .= 'line-height:' . (intval($val->size) + 10) . 'px;';
                $style .= 'color:#' . $val->color . ';';
                $style .= $sty;
                $style .= '} ';

                $tabletstyle .= $container . $val->alias . '{';
                $tabletstyle .= 'font-size:' . $val->tablet . 'px;';
                $tabletstyle .= 'line-height:' . (intval($val->tablet) + 8) . 'px;';
                $tabletstyle .= '} ';

                $mobilestyle .= $container . $val->alias . '{';
                $mobilestyle .= 'font-size:' . $val->mobile . 'px;';
                $mobilestyle .= 'line-height:' . (intval($val->mobile) + 6) . 'px;';
                $mobilestyle .= '} ';
            }
        }
    }
    /* Style */
    $graddiff = 0x81c2d;

    $primary = $general->primarycolor->value;
    $primaryrgb = themeaxe_GetRGB($primary);
    $primarygrad = hexdec('0x' . $primary);
    $primarygrad += $graddiff;
    $primarygrad = dechex($primarygrad);

    $other .= '.primary{color:#' . $primary . ';} .primarybg,.commentcount,input[type="submit"],.readmore,.woocommerce #content input.button, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce-page #content input.button, .woocommerce-page #respond input#submit, .woocommerce-page a.button, .woocommerce-page button.button, .woocommerce-page input.button,.woocommerce #content input.button.alt, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce-page #content input.button.alt, .woocommerce-page #respond input#submit.alt, .woocommerce-page a.button.alt, .woocommerce-page button.button.alt, .woocommerce-page input.button.alt{background-color:#' . $primary . ';}.primaryborder,#footer{border-color:#' . $primary . ';}.primaryrgba{background:rgba(' . $primaryrgb . ',0.75)}.primarygrad{background: #' . $primary . ';background: -moz-linear-gradient(top, #' . $primarygrad . ' 0%, #' . $primary . ' 100%);background: -webkit-linear-gradient(top, #' . $primarygrad . ' 0%,#' . $primary . ' 100%);background: linear-gradient(to bottom, #' . $primarygrad . ' 0%,#' . $primary . ' 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#' . $primarygrad . '\', endColorstr=\'#' . $primary . '\',GradientType=0 );}';

    $secondary = $general->secondarycolor->value;
    $secondaryrgb = themeaxe_GetRGB($secondary);
    $secondarygrad = hexdec('0x' . $secondary);
    $secondarygrad += $graddiff;
    $secondarygrad = dechex($secondarygrad);

    $other .= '.secondary,#topheader ul li a:hover,#topheader a:hover,#maincontent ul li:before{color:#' . $secondary . ';} .secondarybg,.comment-list li.comment.odd,.commentcount:hover,input[type="submit"]:hover,input[type="submit"]:focus,.woocommerce #content input.button:hover, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .woocommerce-page #content input.button:hover, .woocommerce-page #respond input#submit:hover, .woocommerce-page a.button:hover, .woocommerce-page button.button:hover, .woocommerce-page input.button:hover,.woocommerce #content input.button.alt:hover, .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, .woocommerce-page #content input.button.alt:hover, .woocommerce-page #respond input#submit.alt:hover, .woocommerce-page a.button.alt:hover, .woocommerce-page button.button.alt:hover, .woocommerce-page input.button.alt:hover,.woocommerce span.onsale, .woocommerce-page span.onsale,::selection{background-color:#' . $secondary . ';}.secondaryborder{border-color:#' . $secondary . ';}.woocommerce span.onsale, .woocommerce-page span.onsale,.single-product #maincontent div.product .button:hover,#maincontent .woocommerce input.button:hover,.woocommerce .cart-collaterals .shipping_calculator .button:hover, .woocommerce-page .cart-collaterals .shipping_calculator .button:hover,input[type="submit"]:hover,::selection{background:#' . $secondary . '}.secondaryrgba{background:rgba(' . $secondaryrgb . ',0.75)}.secondarygrad{background: #' . $secondary . ';background: -moz-linear-gradient(top, #' . $secondarygrad . ' 0%, #' . $secondary . ' 100%);background: -webkit-linear-gradient(top, #' . $secondarygrad . ' 0%,#' . $secondary . ' 100%);background: linear-gradient(to bottom, #' . $secondarygrad . ' 0%,#' . $secondary . ' 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#' . $secondarygrad . '\', endColorstr=\'#' . $secondary . '\',GradientType=0 );}';

    $tertiary = $general->tertiarycolor->value;
    $tertiaryrgb = themeaxe_GetRGB($tertiary);
    $tertiarygrad = hexdec('0x' . $tertiary);
    $tertiarygrad += $graddiff;
    $tertiarygrad = dechex($tertiarygrad);

    $other .= '.tertiary{color:#'.$tertiary.'}.tertiarybg{background-color:#'.$tertiary.'}.tertiaryborder{border-color:#' . $tertiary . ';}.tertiaryrgba{background:rgba('.$tertiaryrgb.',0.75)}.tertiarygrad{background: #' . $tertiary . ';background: -moz-linear-gradient(top, #' . $tertiarygrad . ' 0%, #' . $tertiary . ' 100%);background: -webkit-linear-gradient(top, #' . $tertiarygrad . ' 0%,#' . $tertiary . ' 100%);background: linear-gradient(to bottom, #' . $tertiarygrad . ' 0%,#' . $tertiary . ' 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#' . $tertiarygrad . '\', endColorstr=\'#' . $tertiary . '\',GradientType=0 );}';

    $style .= $other;

    $width = $general->wrapperwidth->value;
    switch ($general->wrapperstyle->value) {
        case 'fullwidth':
        $style .= '.wrapwidth,.panel-layout > .panel-grid{ width:98vw;}';
        $style .= '@media screen and (max-width:1024px){ .wrapwidth,.panel-layout > .panel-grid{width:94vw;}}';
        break;
        default:
        $style .= '.wrapwidth,.panel-layout > .panel-grid{ width:' . $width . 'px;}';
        $style .= '@media screen and (max-width:' . $width . 'px){ .wrapwidth,.panel-layout > .panel-grid{width:94vw;}}';
        break;
    }

    $style .= '@media screen and (max-width:1024px){' . $tabletstyle . '}';
    $style .= '@media screen and (max-width:680px){' . $mobilestyle . '}';
    $style .= '</style>';

    echo $style;

}
